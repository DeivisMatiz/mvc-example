﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MvcMovie.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/Values")]
    public class ValuesController : Controller
    {

        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            // imagine some db logic
            List<string> values = new List<string>() { "value1", "value2" };
            return Ok(values);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]string value)
        {
            // imagine some db logic
            return Created("", value);
        }      

        // GET: api/Values/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }       
        
        // PUT: api/Values/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]string value)
        {
            return Created("", value);
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(id);
        }
    }
}
