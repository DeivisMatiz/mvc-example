﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMovie.Api.Models
{
    public class TodoItem
    {
        public long Id { get; set; }
        public string MovieName { get; set; }
        public bool IsComplete { get; set; }
    }
}
