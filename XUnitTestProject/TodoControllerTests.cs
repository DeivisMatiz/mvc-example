using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using Xunit;
using MvcMovie.Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Net;

namespace XUnitTestProject
{
    public class TodoControllerTests
    {

        [Fact]
        public async Task TestGet()
        {
            // Arrange
            var controller = new ValuesController();

            // Act
            IActionResult actionResult = await controller.Get();

            // Assert
            Assert.NotNull(actionResult);

            OkObjectResult result = actionResult as OkObjectResult;

            Assert.NotNull(result);

            List<string> messages = result.Value as List<string>;

            Assert.Equal(2, messages.Count);
            Assert.Equal("value1", messages[0]);
            Assert.Equal("value2", messages[1]);
        }

        [Fact]
        public async Task TestPost()
        {
            // Arrange
            var controller = new ValuesController();

            // Act
            IActionResult actionResult = await controller.Post("Some value");

            // Assert
            Assert.NotNull(actionResult);
            CreatedResult result = actionResult as CreatedResult;

            Assert.NotNull(result);
            Assert.Equal(201, result.StatusCode);
        }

        [Fact]
        public async Task TestPut()
        {
            // Arrange
            var controller = new ValuesController();

            // Act
            IActionResult actionResult = await controller.Put(1,"");

            // Assert
            Assert.NotNull(actionResult);
            CreatedResult result = actionResult as CreatedResult;

            Assert.NotNull(result);
            Assert.Equal(201, result.StatusCode);
        }

        [Fact]
        public async Task TestDelete()
        {
            // Arrange
            var controller = new ValuesController();

            // Act
            var actionResult = await controller.Delete(0);

            // Assert
            Assert.NotNull(actionResult);
            var result = actionResult as OkObjectResult;
            Assert.NotNull(result);
            Assert.Equal(200, result.StatusCode);
        }
    }
}
