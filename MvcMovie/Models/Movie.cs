﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace MvcMovie.Models
{
    public class Movie
    {
        public int ID { get; set; }

        //[StringLength(30, MinimumLength = 2)]
        //[Required]
        public string Title { get; set; }

        [Display(Name = "Release Year")]
        [Range(1800, 2200)]
        public int ReleaseYear { get; set; }

        //[RegularExpression(@"^[A-Z]+[a-zA-Z''-'\s]*$")]
        //[Required]
        //[StringLength(30)]
        public string Genre { get; set; }

        //[Range(1, 10)]
        //[Required]
        public int Price { get; set; }
    }
}